# Secure DNS

## Overview

A dockerized setup for Pihole (ad block) + [Mullvad DNS][mullvad-dns] (DoH)
upstream (cloudflared).

## Requirements

- docker
- docker-compose

## Setup (Linux & macOS)

Clone this repo on the computer you want to use as a secure DNS server.

Copy `.env.example` to `.env` and make the appropriate changes.

To start up, run

```shell
docker compose up -d
```

Now you can change your router/device DNS to the server IP.

### Updating `.env`

After making changes to `.env` you'll need to restart the docker network.

```shell
docker compose down && docker compose up -d
```

### Auto start (Linux + macOS)

Add the following to your crontab:

```crontab
@reboot /bin/bash -c "cd <working_path-secure-dns>/ && docker compose up -d"
```

### Upgrading

To upgrade pihole when there's new releases run (_make sure you pull the images
while secure-dns is running_):

```shell
docker compose pull
```

Then restart pihole

```shell
docker compose up -d --remove-orphans
```

### Using secure-dns as your DNS resolver

Once secure-dns is up and running, change your DNS to `127.0.0.1`.
You're done!

[Test your connection for leaks](https://mullvad.net/en/check).

### Auto upgrade with Pullio

If you'd rather not think about upgrading, you can install [pullio][pullio], and
secure-dns should be supported.

## Attribution

### Software

[pi-hole/pi-hole][pihole]

[pi-hole/docker-pi-hole][docker-pihole]

[hotio/pullio][pullio]

### Docker images

[pihole/pihole](https://hub.docker.com/r/pihole/pihole)

[cloudflare/cloudflared](https://hub.docker.com/r/cloudflare/cloudflared)

### Guides

[Pi-hole and cloudflared with Docker][cloudflared]

[mullvad-dns]: https://mullvad.net/en/help/dns-over-https-and-dns-over-tls#specifications
[pihole]: https://github.com/pi-hole/pi-hole
[docker-pihole]: https://github.com/pi-hole/docker-pi-hole
[pullio]: https://hotio.dev/scripts/pullio
[cloudflared]: https://mroach.com/2020/08/pi-hole-and-cloudflared-with-docker/#option-1-hidden-cloudflared
